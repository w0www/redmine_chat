module ChatUsersHelper
  def conversation_interlocutor(conversation)
    conversation.recipient == User.current ? conversation.sender : conversation.recipient
  end
end
