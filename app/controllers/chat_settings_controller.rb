class ChatSettingsController < ApplicationController
  unloadable
  #before_action :find_project_by_project_id, :authorize

  def save
    (render_403; return false) unless @project.module_enabled?(:redmine_chat)
    if params[:chat_settings] && params[:chat_settings].is_a?(Hash) then
      settings = params[:chat_settings]
      settings.map do |k, v|
        ChatSetting[k, @project.id] = v
      end
    end
    redirect_to :controller => 'projects', :action => 'settings', :id => @project, :tab => params[:tab]
  end

end
