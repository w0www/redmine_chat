class ChatMessage < ActiveRecord::Base
  belongs_to :conversation
  belongs_to :user

  validates_presence_of :body, :conversation_id, :user_id
  
  validates_length_of :body, maximum: 1024
end
