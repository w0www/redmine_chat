class ChatSetting < ActiveRecord::Base
  unloadable

  belongs_to :project

  attr_accessible :name, :value, :project_id

  cattr_accessor :settings
  # Hash used to cache setting values
  @chat_cached_settings = {}  
  validates_uniqueness_of :name, :scope => [:project_id]

  # Returns the value of the setting named name
  def self.[](name, project_id)
    project_id = project_id.id if project_id.is_a?(Project)
    v = @chat_cached_settings[hk(name, project_id)]
    v ? v : (@chat_cached_settings[hk(name, project_id)] = find_or_default(name, project_id).value)
  end

  def self.[]=(name, project_id, v)
    project_id = project_id.id if project_id.is_a?(Project)
    setting = find_or_default(name, project_id)
    setting.value = (v ? v : "")
    @chat_cached_settings[hk(name, project_id)] = nil
    setting.save
    setting.value
  end
  
  private

    def self.hk(name, project_id)
      "#{name}-#{project_id.to_s}"
    end
    
    # Returns the Setting instance for the setting named name
    # (record found in database or new record with default value)
    def self.find_or_default(name, project_id)
      name = name.to_s
      setting = find_by_name_and_project_id(name, project_id)
      setting ||= new(:name => name, :value => '', :project_id => project_id)
    end
end
