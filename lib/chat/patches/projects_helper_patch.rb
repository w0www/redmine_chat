require_dependency 'queries_helper'

module RedmineChat
  module Patches
    module ProjectsHelperPatch
      def self.included(base)
        base.send(:include, InstanceMethods)

        base.class_eval do
          unloadable

          alias_method_chain :project_settings_tabs, :chat
        end
      end

      module InstanceMethods
        def project_settings_tabs_with_chat
          tabs = project_settings_tabs_without_chat

          tabs.push({ :name => 'chat',
            :action => :manage_chat,
            :partial => 'projects/chat_settings',
            :label => :label_chat }) if User.current.allowed_to?(:manage_chat, @project)
          tabs
        end
      end

    end
  end
end

unless ProjectsHelper.included_modules.include?(RedmineChat::Patches::ProjectsHelperPatch)
  ProjectsHelper.send(:include, RedmineChat::Patches::ProjectsHelperPatch)
end
