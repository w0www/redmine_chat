module RedmineChat
  module Hooks
    class ViewsLayoutsHook < Redmine::Hook::ViewListener
      render_on :view_layouts_base_content, :partial => "layouts/chat"
      render_on :view_layouts_base_html_head, :partial => "layouts/chat_head"
    end
  end
end
