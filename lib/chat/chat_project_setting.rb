class ChatProjectSetting
  unloadable

  def initialize(project, plugin_name)
    @project = project
    @plugin_settings_name = plugin_name
    Setting["plugin_" + @plugin_settings_name]
  end

  def method_missing(method_name, *args, &block)
    return super if /^(.*=)$/ =~ method_name.to_s
    setting_name = method_name.to_s.gsub(/\?|=/, '')
    setting_value = if ChatSetting[@plugin_settings_name + '_' + setting_name,  @project].blank?
      if ChatSetting.respond_to?(method_name)
        ChatSetting.send(method_name)
      else
        Setting["plugin_" + @plugin_settings_name][setting_name]
      end
    else
      ChatSetting[@plugin_settings_name + '_' + setting_name,  @project]
    end

    if /.*\?$/ =~ method_name.to_s
      setting_value.to_i > 0
    else
      setting_value
    end
  end
end
